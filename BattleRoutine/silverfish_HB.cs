﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using Buddy.Coroutines;
using Triton.Bot;
using Triton.Common;
using Triton.Game;
using Triton.Game.Mapping;

namespace HREngine.Bots
{
    public class Silverfish
    {
        public string versionnumber = "117.178";
        private bool singleLog = false;
        private string botbehave = "noname";
        private bool needSleep = false;

        public Playfield lastpf;
        private Settings sttngs = Settings.Instance;

        private List<Minion> ownMinions = new List<Minion>();
        private List<Minion> enemyMinions = new List<Minion>();
        public List<Minion> heros = new List<Minion>();
        private List<Handmanager.Handcard> handCards = new List<Handmanager.Handcard>();
        private int ownPlayerController = 0;
        private List<string> ownSecretList = new List<string>();
        private Dictionary<int, TAG_CLASS> enemySecretList = new Dictionary<int, TAG_CLASS>();
        private Dictionary<int, IDEnumOwner> LurkersDB = new Dictionary<int, IDEnumOwner>();
        public Dictionary<string, Behavior> BehaviorDB = new Dictionary<string, Behavior>();
        public Dictionary<string, string> BehaviorPath = new Dictionary<string, string>();
        List<HSCard> ETallcards = new List<HSCard>();
        Dictionary<string, int> startDeck = new Dictionary<string, int>();
        Dictionary<CardDB.cardIDEnum, int> turnDeck = new Dictionary<CardDB.cardIDEnum, int>();
        Dictionary<int, extraCard> extraDeck = new Dictionary<int, extraCard>();
        bool noDuplicates = false;
        bool patchesInDeck = false;//帕奇斯在牌库
        
        private int currentMana = 0;
        private int ownMaxMana = 0;
        private int numOptionPlayedThisTurn = 0;
        private int numMinionsPlayedThisTurn = 0;
        private int cardsPlayedThisTurn = 0;
        private int ueberladung = 0;//过载
        private int lockedMana = 0;//锁上

        private int enemyMaxMana = 0;
        
        private string heroname = "";
        private string enemyHeroname = "";

        private CardDB.Card heroAbility = new CardDB.Card();
        private int ownHeroPowerCost = 2;//?
        private bool ownAbilityisReady = false;
        private int enemyHeroPowerCost = 2;

        private Weapon ownWeapon;
        private Weapon enemyWeapon;

        private int anzcards = 0;//卡牌数量
        private int enemyAnzCards = 0;

        private int ownHeroFatigue = 0;//疲劳
        private int enemyHeroFatigue = 0;
        private int ownDecksize = 0;//牌库数量
        private int enemyDecksize = 0;

        private Minion ownHero;
        private Minion enemyHero;

        private int techLevel = 0;
        private int tavernUpgradeCost = 0;
        private int refreshCost = 1;
        private int gTurn = 0;
        private int gTurnStep = 0;
        private int anzOgOwnCThunHpBonus = 0;//克苏恩血量
        private int anzOgOwnCThunAngrBonus = 0;//克苏恩攻击
        private int anzOgOwnCThunTaunt = 0;//克苏恩嘲讽
        private int ownCrystalCore = 0;//任务贼
        private bool ownMinionsCost0 = false;
        private bool LothraxionsPower = false;

        private class extraCard
        {
            public string id;
            public bool isindeck;

            public extraCard(string s, bool b)
            {
                this.id = s;
                this.isindeck = b;
            }
            public void setId(string s)
            {
                this.id = s;
            }
            public void setisindeck(bool b)
            {
                this.isindeck = b;
            }

        }
        
        private static Silverfish instance;

        public static Silverfish Instance
        {
            get { return instance ?? (instance = new Silverfish()); }
        }

        private Silverfish()
        {
            //设置文件路径
            this.singleLog = Settings.Instance.writeToSingleFile;
            Helpfunctions.Instance.ErrorLog("开始启动...");
            string p = "." + System.IO.Path.DirectorySeparatorChar + "Routines" + System.IO.Path.DirectorySeparatorChar + "BattleRoutine" +
                       System.IO.Path.DirectorySeparatorChar + "Silverfish" + System.IO.Path.DirectorySeparatorChar;
            string path = p + "UltimateLogs" + Path.DirectorySeparatorChar;
            Directory.CreateDirectory(path);
            sttngs.setFilePath(p + "Data" + Path.DirectorySeparatorChar);

            if (!singleLog)
            {
                sttngs.setLoggPath(path);
            }
            else
            {
                sttngs.setLoggPath(p);
                sttngs.setLoggFile("UILogg.txt");
                Helpfunctions.Instance.createNewLoggfile();
            }
            setBehavior();
        }
        //设置行为
        private bool setBehavior()
        {
            Type[] types = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.BaseType == typeof(Behavior)).ToArray();
            foreach (var t in types)
            {
                string s = t.Name;
                if (s == "BehaviorMana") continue;
                if (s.Length > 8 && s.Substring(0, 8) == "Behavior")
                {
                    Behavior b = (Behavior)Activator.CreateInstance(t);
                    BehaviorDB.Add(b.BehaviorName(), b);
                }
            }

            string p = Path.Combine("Routines", "BattleRoutine", "Silverfish", "behavior");//路径
            string[] files = Directory.GetFiles(p, "Behavior*.cs", SearchOption.AllDirectories);//获取文件
            int bCount = 0;
            foreach (string file in files)
            {
                bCount++;
                string bPath = Path.GetDirectoryName(file);//获取文件夹名字
                var fullPath = Path.GetFullPath(file);//获取完整路径
                var bNane = Path.GetFileNameWithoutExtension(file).Remove(0, 8);//获取除了Behavior之后的名字
                if (BehaviorDB.ContainsKey(bNane))
                {
                    if (!BehaviorPath.ContainsKey(bNane)) BehaviorPath.Add(bNane, bPath);//加入db
                }
            }

            if (BehaviorDB.Count != BehaviorPath.Count || BehaviorDB.Count != bCount)
            {
                // Helpfunctions.Instance.ErrorLog("Behavior: registered - " + BehaviorDB.Count + ", folders - " + bCount + ", have a path - "
                //     + BehaviorPath.Count + ". These values should be the same. Maybe you have some extra files in the 'custom_behavior' folder.");
                Helpfunctions.Instance.ErrorLog("Behavior: 登记过 - " + BehaviorDB.Count + ", 文件夹 - " + bCount + ", 有路径 - "
                    + BehaviorPath.Count + ". 这些值应该相同. 或许你有其他文件在  'custom_behavior' 文件夹.");
            }

            if (BehaviorDB.ContainsKey("控场模式"))
            {//必须
                Ai.Instance.botBase = BehaviorDB["控场模式"];
                return true;
            }
            else
            {
                Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                Helpfunctions.Instance.ErrorLog("没有找到战场策略!");
                Helpfunctions.Instance.ErrorLog("请把战场策略放到指定的文件中.");
                Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                return false;
            }
        }
        public Behavior getBehaviorByName(string bName)
        {
            if (BehaviorDB.ContainsKey(bName))
            {
                sttngs.setSettings(bName);
                ComboBreaker.Instance.readCombos(bName);
                RulesEngine.Instance.readRules(bName);
                return BehaviorDB[bName];
            }
            else
            {
                if (BehaviorDB.ContainsKey("控场模式")) return BehaviorDB["控场模式"];//默认控场模式
                else
                {
                    Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                    Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                    Helpfunctions.Instance.ErrorLog("没有找到战场策略!");
                    Helpfunctions.Instance.ErrorLog("请把战场策略放到指定的文件中.");
                    Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                    Helpfunctions.Instance.ErrorLog("ERROR#################################################");
                }
            }
            return null;
        }


        public void setnewLoggFile()
        {
            gTurn = 0;
            gTurnStep = 0;
            anzOgOwnCThunHpBonus = 0;
            anzOgOwnCThunAngrBonus = 0;
            anzOgOwnCThunTaunt = 0;
            ownCrystalCore = 0;
            ownMinionsCost0 = false;
            Questmanager.Instance.Reset();
            if (!singleLog)
            {
                sttngs.setLoggFile("UILogg" + DateTime.Now.ToString("_yyyy-MM-dd_HH-mm-ss") + ".txt");
                Helpfunctions.Instance.createNewLoggfile();
                Helpfunctions.Instance.ErrorLog("#######################################################");
                Helpfunctions.Instance.ErrorLog("对局日志保持在: " + sttngs.logpath + sttngs.logfile);
                Helpfunctions.Instance.ErrorLog("#######################################################");
            }
            else
            {
                sttngs.setLoggFile("UILogg.txt");
            }
            //读取信息加入卡组
            startDeck.Clear();
            extraDeck.Clear();
        }

        public bool updateEverything(Behavior botbase, int numcal, out bool sleepRetry)
        {//更新所有信息
            this.needSleep = false;
            this.updateBehaveString(botbase);
            ownPlayerController = TritonHs.OurHero.ControllerId;

            Hrtprozis.Instance.clearAllRecalc();
            Handmanager.Instance.clearAllRecalc();
            
            getHerostuff();//获得 英雄状态
            getMinions();//获得 随从
            getHandcards();//获得 手牌
            getDecks();//获得 卡组
            getTechs();//获得等级
            getHeros();//获取英雄


            Hrtprozis.Instance.updateTurnDeck(turnDeck, noDuplicates, patchesInDeck);//对局信息更新卡组
            
            Hrtprozis.Instance.setOwnPlayer(ownPlayerController);//对局信息设置我方玩家                
            Handmanager.Instance.setOwnPlayer(ownPlayerController);//手牌管理 设置我方玩家

            this.numOptionPlayedThisTurn = 0;//本回合选项数量
            this.numOptionPlayedThisTurn += this.cardsPlayedThisTurn;//打出的卡片数量+攻击数量
            
            List<HSCard> list = TritonHs.GetCards(CardZone.Graveyard, true);//本回合选项数量增加
            foreach (GraveYardItem gi in Probabilitymaker.Instance.turngraveyard)//墓地
            {
                if (gi.own)
                {
                    foreach (HSCard entiti in list)
                    {
                        if (gi.entity == entiti.EntityId)
                        {
                            this.numOptionPlayedThisTurn += entiti.NumAttackThisTurn;
                            break;
                        }
                    }
                }
            }

            Hrtprozis.Instance.updatePlayer(this.ownMaxMana, this.currentMana, this.cardsPlayedThisTurn,
                this.numMinionsPlayedThisTurn, this.numOptionPlayedThisTurn, this.ueberladung, this.lockedMana, TritonHs.OurHero.EntityId,
                TritonHs.EnemyHero.EntityId);//更新玩家

            // 更新瞎子技能
            //if (this.heroAbility != null)
            //{
            //    if (this.heroAbility.cardIDenum == CardDB.cardIDEnum.HERO_10bp || this.heroAbility.cardIDenum == CardDB.cardIDEnum.HERO_10bp2)
            //    {
            //        this.ownHeroPowerCost = 1;
            //    }
            //}

            Hrtprozis.Instance.updateHero(this.ownWeapon, this.heroname, this.heroAbility, this.ownAbilityisReady, this.ownHeroPowerCost, this.ownHero);//更新我方英雄
            Hrtprozis.Instance.updateHero(this.enemyWeapon, this.enemyHeroname, null, false, this.enemyHeroPowerCost, this.enemyHero, this.enemyMaxMana);//更新敌方英雄

            Hrtprozis.Instance.updatePlayerAttachments(this.LothraxionsPower);

            Questmanager.Instance.updatePlayedMobs(this.gTurnStep);//任务管理
            Hrtprozis.Instance.updateMinions(this.ownMinions, this.enemyMinions);//更新随从
            Handmanager.Instance.setHandcards(this.handCards, this.anzcards, this.enemyAnzCards);//手牌
            Hrtprozis.Instance.updateTurnInfo(this.gTurn, this.gTurnStep);//回合
            Hrtprozis.Instance.updateTechInfo(this.techLevel, this.tavernUpgradeCost, this.refreshCost);

            sleepRetry = this.needSleep;
            if (sleepRetry && numcal == 0) return false;


            if (!Hrtprozis.Instance.setGameRule)
            {//不设置游戏规则
                RulesEngine.Instance.setCardIdRulesGame(this.ownHero.cardClass, this.enemyHero.cardClass);
                Hrtprozis.Instance.setGameRule = true;
            }

            Playfield p = new Playfield();
            p.enemyCardsOut = new Dictionary<CardDB.cardIDEnum, int>(Probabilitymaker.Instance.enemyCardsOut);//敌方出牌

            if (lastpf != null)
            {
                if (lastpf.isEqualf(p))
                {
                    return false;
                }
                else
                {
                     if (p.gTurnStep > 0 && Ai.Instance.nextMoveGuess.ownMinions.Count == p.ownMinions.Count)
                     {
                         if (Ai.Instance.nextMoveGuess.ownHero.Ready != p.ownHero.Ready && !p.ownHero.Ready)
                         {
                             sleepRetry = true;
                             Helpfunctions.Instance.ErrorLog("[AI] 英雄的准备状态 = " + p.ownHero.Ready + ". 再次尝试....");
                             Ai.Instance.nextMoveGuess = new Playfield { mana = -100 };
                             return false;
                         }                         
                         for (int i = 0; i < p.ownMinions.Count; i++)
                         {
                             if (Ai.Instance.nextMoveGuess.ownMinions[i].Ready != p.ownMinions[i].Ready && !p.ownMinions[i].Ready)
                             {
                                 sleepRetry = true;
                                 Helpfunctions.Instance.ErrorLog("[AI] 随从的准备状态 = " + p.ownMinions[i].Ready + " (" + p.ownMinions[i].entitiyID + " " + p.ownMinions[i].handcard.card.cardIDenum + " " + p.ownMinions[i].name + "). 再次尝试....");
                                 Ai.Instance.nextMoveGuess = new Playfield { mana = -100 };
                                 return false;
                             }
                         }                         
                     }
                }
                
                lastpf = p;//存储上一次场面信息
            }
            else
            {
                lastpf = p;
            }

            p = new Playfield(); 
            printUtils.printNowVal();
            
            Helpfunctions.Instance.ErrorLog("AI计算中，请稍候... " + DateTime.Now.ToString("HH:mm:ss.ffff"));
            
            
            using (TritonHs.Memory.ReleaseFrame(true))
            {
                printstuff();  //打印牌面细节到logg
                Ai.Instance.dosomethingclever(botbase);    //核心Ai计算
            }

            Helpfunctions.Instance.ErrorLog("计算完成! " + DateTime.Now.ToString("HH:mm:ss.ffff"));
            if (sttngs.printRules > 0)
            {
                String[] rulesStr = Ai.Instance.bestplay.rulesUsed.Split('@');
                if (rulesStr.Count() > 0 && rulesStr[0] != "")
                {
                    Helpfunctions.Instance.ErrorLog("规则权重 " + Ai.Instance.bestplay.ruleWeight * -1);
                    foreach (string rs in rulesStr)
                    {
                        if (rs == "") continue;
                        Helpfunctions.Instance.ErrorLog("规则: " + rs);
                    }
                }
            }
            return true;
        }




        private void getHerostuff()//获得英雄材料
        {
            List<HSCard> allcards = TritonHs.GetAllCards();

            HSCard ownHeroCard = TritonHs.OurHero;
            HSCard enemHeroCard = TritonHs.EnemyHero;
            int ownheroentity = TritonHs.OurHero.EntityId;
            int enemyheroentity = TritonHs.EnemyHero.EntityId;
            this.ownHero = new Minion();
            this.enemyHero = new Minion();

            foreach (HSCard ent in allcards)
            {
                if (ent.IsHero == true)
                {
                    if (ent.ControllerId == 1 && this.ownHero.cardClass == TAG_CLASS.INVALID)
                    {
                        this.ownHero.cardClass = (TAG_CLASS)ent.Class;

                    }
                    if (ent.ControllerId == 2 && this.enemyHero.cardClass == TAG_CLASS.INVALID)
                    {
                        this.enemyHero.cardClass = (TAG_CLASS)ent.Class;

                    }
                    if (ent.EntityId == enemyheroentity) enemHeroCard = ent;
                    if (ent.EntityId == ownheroentity) ownHeroCard = ent;
                }
            }

            
            
            this.currentMana = TritonHs.CurrentMana;
            this.ownMaxMana = TritonHs.Resources;
            this.enemyMaxMana = ownMaxMana;
            //奇数骑 新传说
            this.LothraxionsPower = false;
            var attachments = GameState.Get().GetFriendlySidePlayer().GetAttachments();
            if(attachments.Count > 0)
            {
                foreach(var attachment in attachments)
                {
                    switch(attachment.GetCardId())
                    {
                        case "DMF_240e": this.LothraxionsPower = true;break;
                    }
                }
            }
            

            
            ownSecretList.Clear();
            enemySecretList.Clear();
            foreach (HSCard ent in allcards)
            {
                if (ent.IsSecret && ent.ControllerId != ownPlayerController && ent.GetTag(GAME_TAG.ZONE) == 7)
                {
                    enemySecretList.Add(ent.EntityId, (TAG_CLASS)ent.Class);
                }
                if (ent.IsSecret && ent.ControllerId == ownPlayerController && ent.GetTag(GAME_TAG.ZONE) == 7)
                {
                    ownSecretList.Add(ent.Id);
                }
            }

            var ownSecretZoneCards = GameState.Get().GetFriendlySidePlayer().GetSecretZone().GetCards();
            foreach (var card in ownSecretZoneCards)
            {
                var entity = card.GetEntity();
                if (entity != null && entity.GetZone() == Triton.Game.Mapping.TAG_ZONE.SECRET)
                {
                    if (entity.m_card != null)
                    {
                        string eId = "";



                        
                        if (entity.IsQuest())
                        {
                            int currentQuestProgress = entity.GetTag(GAME_TAG.QUEST_PROGRESS);
                            int questProgressTotal = entity.GetTag(GAME_TAG.QUEST_PROGRESS_TOTAL);

                            var entityDef = DefLoader.Get().GetEntityDef(entity.GetTag(GAME_TAG.QUEST_CONTRIBUTOR).ToString());
                            if (entityDef != null)
                            {
                                var nme = entityDef.GetName();
                            }

                            Questmanager.Instance.updateQuestStuff(eId, currentQuestProgress, questProgressTotal, true);
                        }
                    }
                }
            }

            var enemySecretZoneCards = GameState.Get().GetOpposingSidePlayer().GetSecretZone().GetCards();
            foreach (var card in enemySecretZoneCards)
            {
                var entity = card.GetEntity();
                if (entity != null && entity.GetZone() == Triton.Game.Mapping.TAG_ZONE.SECRET)
                {
                    if (entity.m_card != null)
                    {
                        string eId = "";



                        
                        if (entity.IsQuest())
                        {
                            int currentQuestProgress = entity.GetTag(GAME_TAG.QUEST_PROGRESS);
                            int questProgressTotal = entity.GetTag(GAME_TAG.QUEST_PROGRESS_TOTAL);
                            Questmanager.Instance.updateQuestStuff(eId, currentQuestProgress, questProgressTotal, false);
                        }
                    }
                }
            }
            
            numMinionsPlayedThisTurn = TritonHs.NumMinionsPlayedThisTurn;
            cardsPlayedThisTurn = TritonHs.NumCardsPlayedThisTurn;
            ueberladung = TritonHs.OverloadOwed;
            lockedMana = TritonHs.OverloadLocked;

            this.ownHeroFatigue = ownHeroCard.GetTag(GAME_TAG.FATIGUE);
            this.enemyHeroFatigue = enemHeroCard.GetTag(GAME_TAG.FATIGUE);

            this.ownDecksize = 0;
            this.enemyDecksize = 0;
            
            foreach (HSCard ent in allcards)
            {
                if (ent.ControllerId == ownPlayerController && ent.GetTag(GAME_TAG.ZONE) == 2) ownDecksize++;
                if (ent.ControllerId != ownPlayerController && ent.GetTag(GAME_TAG.ZONE) == 2) enemyDecksize++;
            }

            this.heroname = Hrtprozis.Instance.heroIDtoName(TritonHs.OurHero.Id);

            this.ownHero.Angr = ownHeroCard.GetTag(GAME_TAG.ATK);
            this.ownHero.Hp = ownHeroCard.GetTag(GAME_TAG.HEALTH) - ownHeroCard.GetTag(GAME_TAG.DAMAGE);
            this.ownHero.armor = ownHeroCard.GetTag(GAME_TAG.ARMOR);            
            this.ownHero.frozen = (ownHeroCard.GetTag(GAME_TAG.FROZEN) == 0) ? false : true;
            this.ownHero.stealth = (ownHeroCard.GetTag(GAME_TAG.STEALTH) == 0) ? false : true;
            this.ownHero.windfury = (ownHeroCard.GetTag(GAME_TAG.WINDFURY) == 0) ? false : true;
            this.ownHero.immune = (ownHeroCard.GetTag(GAME_TAG.IMMUNE) == 0) ? false : true;
            if (!ownHero.immune) this.ownHero.immune = (ownHeroCard.GetTag(GAME_TAG.IMMUNE_WHILE_ATTACKING) == 0) ? false : true; //turn immune
            
            ownWeapon = new Weapon();
            if (TritonHs.DoWeHaveWeapon)//如果我们有武器
            {
                HSCard weapon = TritonHs.OurWeaponCard;

                ownWeapon.equip(CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(weapon.Id)));
                ownWeapon.Angr = weapon.GetTag(GAME_TAG.ATK);
                ownWeapon.Durability = weapon.GetTag(GAME_TAG.DURABILITY) - weapon.GetTag(GAME_TAG.DAMAGE);
                ownWeapon.poisonous = (weapon.GetTag(GAME_TAG.POISONOUS) == 1) ? true : false;
                ownWeapon.lifesteal = (weapon.GetTag(GAME_TAG.LIFESTEAL) == 1) ? true : false;
                //武器法术迸发
                ownWeapon.Spellburst = (weapon.GetTag(GAME_TAG.SPELLBURST) == 1) ? true : false;
                if (!this.ownHero.windfury) this.ownHero.windfury = ownWeapon.windfury;
            }
                        
            this.enemyHeroname = Hrtprozis.Instance.heroIDtoName(TritonHs.EnemyHero.Id);

            this.enemyHero.Angr = enemHeroCard.GetTag(GAME_TAG.ATK);
            this.enemyHero.Hp = enemHeroCard.GetTag(GAME_TAG.HEALTH) - enemHeroCard.GetTag(GAME_TAG.DAMAGE);
            this.enemyHero.armor = enemHeroCard.GetTag(GAME_TAG.ARMOR);
            this.enemyHero.frozen = (enemHeroCard.GetTag(GAME_TAG.FROZEN) == 0) ? false : true;
            this.enemyHero.stealth = (enemHeroCard.GetTag(GAME_TAG.STEALTH) == 0) ? false : true;
            this.enemyHero.windfury = (enemHeroCard.GetTag(GAME_TAG.WINDFURY) == 0) ? false : true;
            this.enemyHero.immune = (enemHeroCard.GetTag(GAME_TAG.IMMUNE) == 0) ? false : true; //don't use turn immune
            
            enemyWeapon = new Weapon();
            if (TritonHs.DoesEnemyHasWeapon)//对手有武器
            {
                HSCard weapon = TritonHs.EnemyWeaponCard;

                enemyWeapon.equip(CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(weapon.Id)));
                enemyWeapon.Angr = weapon.GetTag(GAME_TAG.ATK);
                enemyWeapon.Durability = weapon.GetTag(GAME_TAG.DURABILITY) - weapon.GetTag(GAME_TAG.DAMAGE);
                enemyWeapon.poisonous = (weapon.GetTag(GAME_TAG.POISONOUS) == 1) ? true : false;
                enemyWeapon.lifesteal = (weapon.GetTag(GAME_TAG.LIFESTEAL) == 1) ? true : false;
                enemyWeapon.Spellburst = (weapon.GetTag(GAME_TAG.SPELLBURST) == 1) ? true : false;
                ownWeapon.Spellburst = (weapon.GetTag(GAME_TAG.SPELLBURST) == 1) ? true : false;
                if (!this.enemyHero.windfury) this.enemyHero.windfury = enemyWeapon.windfury;
            }
            
            this.heroAbility =
                CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(TritonHs.OurHeroPowerCard.Id));
            this.ownAbilityisReady = (TritonHs.OurHeroPowerCard.GetTag(GAME_TAG.EXHAUSTED) == 0) ? true : false;
            int ownHeroAbilityEntity = TritonHs.OurHeroPowerCard.EntityId;
            
            int needBreak = 0;
            foreach (HSCard ent in allcards)
            {
                if (ent.EntityId == ownHeroAbilityEntity)
                {
                    this.ownHeroPowerCost = ent.Cost;
                    needBreak++;
                }
                if (needBreak > 1) break;
            }
            
            this.ownHero.isHero = true;
            this.enemyHero.isHero = true;
            this.ownHero.own = true;
            this.enemyHero.own = false;
            this.ownHero.maxHp = ownHeroCard.GetTag(GAME_TAG.HEALTH);
            this.enemyHero.maxHp = enemHeroCard.GetTag(GAME_TAG.HEALTH);
            this.ownHero.entitiyID = ownHeroCard.EntityId;
            this.enemyHero.entitiyID = enemHeroCard.EntityId;

            this.ownHero.Ready = ownHeroCard.CanBeUsed;
            this.enemyHero.Ready = false;
            
            List<miniEnch> miniEnchlist = new List<miniEnch>();
            foreach (HSCard ent in allcards)
            {
                if (ent.GetTag(GAME_TAG.ATTACHED) == this.ownHero.entitiyID && ent.GetTag(GAME_TAG.ZONE) == 1) 
                {
                    CardDB.cardIDEnum id = CardDB.Instance.cardIdstringToEnum(ent.Id);
                    int controler = ent.GetTag(GAME_TAG.CONTROLLER);
                    int creator = ent.GetTag(GAME_TAG.CREATOR);
                    //int copyDeathrattle = ent.GetTag(GAME_TAG.COPY_DEATHRATTLE);
                    miniEnchlist.Add(new miniEnch(id, creator, controler));
                }

            }

            this.ownHero.loadEnchantments(miniEnchlist, ownHeroCard.GetTag(GAME_TAG.CONTROLLER));

            miniEnchlist.Clear();

            foreach (HSCard ent in allcards)
            {
                if (ent.GetTag(GAME_TAG.ATTACHED) == this.enemyHero.entitiyID && ent.GetTag(GAME_TAG.ZONE) == 1)
                    
                {
                    CardDB.cardIDEnum id = CardDB.Instance.cardIdstringToEnum(ent.Id);
                    int controler = ent.GetTag(GAME_TAG.CONTROLLER);
                    int creator = ent.GetTag(GAME_TAG.CREATOR);
                    //int copyDeathrattle = ent.GetTag(GAME_TAG.COPY_DEATHRATTLE);
                    miniEnchlist.Add(new miniEnch(id, creator, controler));
                }

            }

            this.enemyHero.loadEnchantments(miniEnchlist, enemHeroCard.GetTag(GAME_TAG.CONTROLLER));

            if (this.ownHero.Angr < this.ownWeapon.Angr) this.ownHero.Angr = this.ownWeapon.Angr;
            if (this.enemyHero.Angr < this.enemyWeapon.Angr) this.enemyHero.Angr = this.enemyWeapon.Angr;
        }



        private void getMinions()//随从效果
        {
            int tmp = Triton.Game.Mapping.GameState.Get().GetTurn();
            if (gTurn == tmp) gTurnStep++;
            else gTurnStep = 0;
            gTurn = tmp;
            
            List<HSCard> list = TritonHs.GetCards(CardZone.Battlefield, true);
            list.AddRange(TritonHs.GetCards(CardZone.Battlefield, false));

            var enchantments = new List<HSCard>();
            ownMinions.Clear();
            enemyMinions.Clear();
            LurkersDB.Clear();
            List<HSCard> allcards = TritonHs.GetAllCards();

            foreach (HSCard entiti in list)
            {
                int zp = entiti.GetTag(GAME_TAG.ZONE_POSITION);

                if (entiti.IsMinion && zp >= 1)
                {

                    HSCard entitiy = entiti;

                    foreach (HSCard ent in allcards)
                    {
                        if (ent.EntityId == entiti.EntityId)
                        {
                            entitiy = ent;
                            break;
                        }
                    }


                    CardDB.Card c = CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(entitiy.Id));
                    Minion m = new Minion();
                    m.name = c.name;
                    m.handcard.card = c;

                    m.cardRace = (HREngine.Bots.TAG_RACE)entitiy.Race;
                    m.cardClass = (HREngine.Bots.TAG_CLASS)entitiy.Class;
                    m.Angr = entitiy.GetTag(GAME_TAG.ATK);
                    m.maxHp = entitiy.GetTag(GAME_TAG.HEALTH);
                    m.Hp = entitiy.GetTag(GAME_TAG.HEALTH) - entitiy.GetTag(GAME_TAG.DAMAGE);
                    if (m.Hp <= 0) continue;
                    m.wounded = false;
                    if (m.maxHp > m.Hp) m.wounded = true;

                    int ctarget = entitiy.GetTag(GAME_TAG.CARD_TARGET);
                    if (ctarget > 0)
                    {
                        foreach (HSCard hcard in allcards)
                        {
                            if (hcard.EntityId == ctarget)
                            {
                                LurkersDB.Add(entitiy.EntityId, new IDEnumOwner()
                                {
                                    IDEnum = CardDB.Instance.cardIdstringToEnum(hcard.Id),
                                    own = (hcard.GetTag(GAME_TAG.CONTROLLER) == this.ownPlayerController) ? true : false
                                });
                                break;
                            }
                        }
                    }

                    m.exhausted = (entitiy.GetTag(GAME_TAG.EXHAUSTED) == 0) ? false : true;//枯竭的

                    m.taunt = (entitiy.GetTag(GAME_TAG.TAUNT) == 0) ? false : true;//嘲讽

                    int temp = entitiy.GetTag(GAME_TAG.NUM_TURNS_IN_PLAY);
                    m.playedThisTurn = (temp == 0) ? true : false;

                    m.Spellburst = entitiy.GetTag(GAME_TAG.SPELLBURST) != 0;//法力迸发

                    m.frenzy = entitiy.GetTag(GAME_TAG.FRENZY) != 0;//暴怒

                    m.windfury = (entitiy.GetTag(GAME_TAG.WINDFURY) == 0) ? false : true;//风怒

                    m.frozen = (entitiy.GetTag(GAME_TAG.FROZEN) == 0) ? false : true;//冰冻

                    m.divineshild = (entitiy.GetTag(GAME_TAG.DIVINE_SHIELD) == 0) ? false : true;//圣盾

                    m.stealth = (entitiy.GetTag(GAME_TAG.STEALTH) == 0) ? false : true;//潜行

                    m.poisonous = (entitiy.GetTag(GAME_TAG.POISONOUS) == 0) ? false : true;//剧毒

                    m.lifesteal = (entitiy.GetTag(GAME_TAG.LIFESTEAL) == 0) ? false : true;//吸血

                    m.immune = (entitiy.GetTag(GAME_TAG.IMMUNE) == 0) ? false : true;//免疫
                    if (!m.immune) m.immune = (entitiy.GetTag(GAME_TAG.IMMUNE_WHILE_ATTACKING) == 0) ? false : true;

                    m.untouchable = (entitiy.GetTag(GAME_TAG.UNTOUCHABLE) == 0) ? false : true;//无法选择

                    m.cantAttackHeroes = (entitiy.GetTag(GAME_TAG.CANNOT_ATTACK_HEROES) == 0) ? false : true;//无法攻击英雄

                    m.cantAttack = (entitiy.GetTag(GAME_TAG.CANT_ATTACK) == 0) ? false : true;//无法攻击

                    m.charge = entitiy.HasCharge ? 1 : 0;//冲锋

                    m.rush = entitiy.HasRush ? 1 : 0;//突袭

                    m.zonepos = zp;//棋盘上的位置

                    m.entitiyID = entitiy.EntityId;//实体id

                    m.hChoice = entitiy.GetTag(GAME_TAG.HIDDEN_CHOICE);

                    List<miniEnch> enchs = new List<miniEnch>();
                    foreach (HSCard ent in allcards)
                    {
                        if (ent.GetTag(GAME_TAG.ATTACHED) == m.entitiyID && ent.GetTag(GAME_TAG.ZONE) == 1) 
                        {
                            CardDB.cardIDEnum id = CardDB.Instance.cardIdstringToEnum(ent.Id);
                            int controler = ent.GetTag(GAME_TAG.CONTROLLER);
                            int creator = ent.GetTag(GAME_TAG.CREATOR);
                            //int copyDeathrattle = ent.GetTag(GAME_TAG.COPY_DEATHRATTLE);
                            enchs.Add(new miniEnch(id, creator, controler));
                        }
                    }
                    
                    m.loadEnchantments(enchs, entitiy.GetTag(GAME_TAG.CONTROLLER));
                    if (m.extraParam2 != 0)
                    {
                        bool needBreak = false;
                        foreach (HSCard ent in allcards)
                        {
                            if (m.extraParam2 == ent.EntityId)
                            {
                                //int copyDeathrattle = ent.GetTag(GAME_TAG.COPY_DEATHRATTLE);
                                switch (ent.Id)
                                {
                                    case "LOE_019"://石化迅猛龙
                                    /*
                                        foreach (HSCard ent2 in allcards)
                                        {
                                            
                                            if (copyDeathrattle == ent2.EntityId)
                                            {
                                                if (ent2.Id == "LOE_019")
                                                {
                                                    //copyDeathrattle = ent2.GetTag(GAME_TAG.COPY_DEATHRATTLE);
                                                    goto case "LOE_019";
                                                }
                                                m.deathrattle2 = CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(ent2.Id));
                                                needBreak = true;
                                                break;
                                            }
                                            
                                        }
                                        */
                                        break;
                                    default:
                                        m.deathrattle2 = CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(ent.Id));
                                        needBreak = true;
                                        break;
                                }
                            }
                            if (needBreak) break;
                        }
                    }

                    m.dormant = entitiy.GetTag(GAME_TAG.DORMANT);//休眠
                    if (m.dormant > 0)
                    {
                        m.untouchable = true;
                        bool flag = false;
                        foreach (var en in entitiy.AttachedCards)
                        {
                            if (en.Id == "BT_737e")
                            {
                                m.dormant = en.GetTag(GAME_TAG.SCORE_VALUE_1) - en.GetTag(GAME_TAG.SCORE_VALUE_2);
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) Helpfunctions.Instance.logg("未能读取到休眠信息");
                    }

                    m.Ready = entitiy.CanBeUsed;

                    if (m.charge > 0 && m.playedThisTurn && !m.Ready)
                    {
                        needSleep = true;
                        Helpfunctions.Instance.ErrorLog("[AI] 冲锋的随从还没有准备好");
                                 
                    }

                    if (entitiy.GetTag(GAME_TAG.CONTROLLER) == this.ownPlayerController) 
                    {
                        m.own = true;                        
                        m.synergy = PenalityManager.Instance.getClassRacePriorityPenality(this.ownHero.cardClass, (TAG_RACE)c.race);
                        this.ownMinions.Add(m);
                    }
                    else
                    {
                        m.own = false;
                        m.synergy = PenalityManager.Instance.getClassRacePriorityPenality(this.enemyHero.cardClass, (TAG_RACE)c.race);
                        this.enemyMinions.Add(m);
                    }
                }
            }

        }

        private void getHandcards()
        {
            handCards.Clear();
            anzcards = 0;
            enemyAnzCards = 0;
            List<HSCard> list = TritonHs.GetCards(CardZone.Hand);
            
            foreach (HSCard entitiy in list)
            {
                if (entitiy.ZonePosition >= 1) 
                {
                    CardDB.Card c = CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(entitiy.Id));
                    
                    Handmanager.Handcard hc = new Handmanager.Handcard();
                    hc.card = c;
                    hc.position = entitiy.ZonePosition;
                    hc.entity = entitiy.EntityId;
                    hc.manacost = entitiy.Cost;
                    // hc.elemPoweredUp = entitiy.GetTag(GAME_TAG.ELEMENTAL_POWERED_UP);
                    
                    hc.addattack = entitiy.Attack - entitiy.DefATK;
                    if (entitiy.IsWeapon) hc.addHp = entitiy.Durability - entitiy.DefDurability;
                    else hc.addHp = entitiy.Health - entitiy.DefHealth;

                    handCards.Add(hc);
                    anzcards++;
                }
            }
            // Hrtprozis.Instance.updateElementals(0, wereElementalsLastTurn, 0); //TODO

            List<HSCard> allcards = TritonHs.GetAllCards();
            enemyAnzCards = 0;
            foreach (HSCard hs in allcards)
            {
                if (hs.GetTag(GAME_TAG.ZONE) == 3 && hs.ControllerId != ownPlayerController &&
                    hs.GetTag(GAME_TAG.ZONE_POSITION) >= 1) enemyAnzCards++;
            }
            
        }

        private void getHeros()
        {
            heros.Clear();
            
            List<HSCard> map = TritonHs.GetPlayerMap;

            foreach (HSCard entitiy in map)
            {
                CardDB.Card c = CardDB.Instance.getCardDataFromID(CardDB.Instance.cardIdstringToEnum(entitiy.Id));
                Minion m = new Minion();
                m.name = c.name;
                m.handcard.card = c;
                m.zonepos = -1;

                m.Angr = entitiy.GetTag(GAME_TAG.ATK);
                m.maxHp = entitiy.GetTag(GAME_TAG.HEALTH);
                m.Hp = entitiy.GetTag(GAME_TAG.HEALTH) - entitiy.GetTag(GAME_TAG.DAMAGE);
                if (m.Hp <= 0) continue;
                m.wounded = false;
                if (m.maxHp > m.Hp) m.wounded = true;
                heros.Add(m);
            }
        }

        public void getTechs()
        {
            this.techLevel = TritonHs.GetTechLevel(true);
            this.tavernUpgradeCost = TritonHs.GetTavernUpgradeCost();
            this.refreshCost = TritonHs.GetTavernRefreshCost();
        }

        public void getDecks()
        {
            Dictionary<string, int> tmpDeck = new Dictionary<string, int>(startDeck);
            List<GraveYardItem> graveYard = new List<GraveYardItem>();
            Dictionary<CardDB.cardIDEnum, int> og = new Dictionary<CardDB.cardIDEnum, int>();
            Dictionary<CardDB.cardIDEnum, int> eg = new Dictionary<CardDB.cardIDEnum, int>();
            int owncontroler = TritonHs.OurHero.GetTag(GAME_TAG.CONTROLLER);
            int enemycontroler = TritonHs.EnemyHero.GetTag(GAME_TAG.CONTROLLER);
            turnDeck.Clear();
            noDuplicates = false;
            patchesInDeck = false;
            
            List<HSCard> allcards = TritonHs.GetAllCards();
            
            int allcardscount = allcards.Count;
            for (int i = 0; i < allcardscount; i++)
            {
                HSCard entity = allcards[i];
                if (entity.Id == null || entity.Id == "") continue;

                // if (CardDB.Instance.cardIdstringToEnum(entity.Id) == CardDB.cardIDEnum.UNG_116t) ownMinionsCost0 = true;

                if (entity.GetZone() == Triton.Game.Mapping.TAG_ZONE.GRAVEYARD)
                {
                    CardDB.cardIDEnum cide = CardDB.Instance.cardIdstringToEnum(entity.Id);
                    GraveYardItem gyi = new GraveYardItem(cide, entity.EntityId, entity.GetTag(GAME_TAG.CONTROLLER) == owncontroler);
                    graveYard.Add(gyi);

                    if (entity.GetTag(GAME_TAG.CONTROLLER) == owncontroler)
                    {
                        if (og.ContainsKey(cide)) og[cide]++;
                        else og.Add(cide, 1);
                    }
                    else if (entity.GetTag(GAME_TAG.CONTROLLER) == enemycontroler)
                    {
                        if (eg.ContainsKey(cide)) eg[cide]++;
                        else eg.Add(cide, 1);
                    }
                    if (cide == CardDB.cardIDEnum.UNG_067t1) ownCrystalCore = 5;
                }

                string entityId = entity.Id;
                Triton.Game.Mapping.TAG_ZONE entZone =  entity.GetZone();
                if (i < 30)
                {
                    if (entityId != "")
                    {
                        if (entZone == Triton.Game.Mapping.TAG_ZONE.DECK) continue;
                        if (tmpDeck.ContainsKey(entityId)) tmpDeck[entityId]--;
                    }
                }
                else if (i >= 60 && entity.ControllerId == owncontroler)
                {
                    if (extraDeck.ContainsKey(i))
                    {
                        if (entityId != "" && entityId != extraDeck[i].id) extraDeck[i].setId(entityId);
                        if ((entZone == Triton.Game.Mapping.TAG_ZONE.DECK) != extraDeck[i].isindeck) extraDeck[i].setisindeck(entZone == Triton.Game.Mapping.TAG_ZONE.DECK);
                    }
                    else if (entZone == Triton.Game.Mapping.TAG_ZONE.DECK)
                    {
                        extraDeck.Add(i, new extraCard(entityId, true));
                    }
                }
            }

            Action a = Ai.Instance.bestmove;
            foreach (var c in extraDeck)
            {
                if (c.Value.isindeck == false) continue;
                CardDB.cardIDEnum ce;
                string entityId = c.Value.id;
                if (entityId == "")
                {
                    continue;
                }
                c.Value.setId(entityId);
                ce = CardDB.Instance.cardIdstringToEnum(entityId);
                if (turnDeck.ContainsKey(ce)) turnDeck[ce]++;
                else turnDeck.Add(ce, 1);
            }
            foreach (var c in tmpDeck)
            {
                if (c.Value < 1) continue;
                CardDB.cardIDEnum ce = CardDB.Instance.cardIdstringToEnum(c.Key);
                if (ce == CardDB.cardIDEnum.None) continue;
                if (turnDeck.ContainsKey(ce)) turnDeck[ce] += c.Value;
                else turnDeck.Add(ce, c.Value);
            }

            Probabilitymaker.Instance.setOwnCardsOut(og);
            Probabilitymaker.Instance.setEnemyCardsOut(eg);
            bool isTurnStart = false;
            if (Ai.Instance.nextMoveGuess.mana == -100)
            {
                isTurnStart = true;
                Ai.Instance.updateTwoTurnSim();
            }
            Probabilitymaker.Instance.setGraveYard(graveYard, isTurnStart);

            

            if (startDeck.Count == 0) return;
            noDuplicates = true;
            foreach (int i in turnDeck.Values)
            {
                if (i > 1)
                {
                    noDuplicates = false;
                    break;
                }
            }
		}

        private void updateBehaveString(Behavior botbase)
        {
            this.botbehave = botbase.BehaviorName();
            this.botbehave += " " + Ai.Instance.maxwide;
            this.botbehave += " face " + ComboBreaker.Instance.attackFaceHP;
            if (Settings.Instance.berserkIfCanFinishNextTour > 0) this.botbehave += " berserk:" + Settings.Instance.berserkIfCanFinishNextTour;
            if (Settings.Instance.weaponOnlyAttackMobsUntilEnfacehp > 0) this.botbehave += " womob:" + Settings.Instance.weaponOnlyAttackMobsUntilEnfacehp;
            if (Settings.Instance.secondTurnAmount > 0)
            {
                if (Ai.Instance.nextMoveGuess.mana == -100)
                {
                    Ai.Instance.updateTwoTurnSim();
                }
                this.botbehave += " twoturnsim " + Settings.Instance.secondTurnAmount + " ntss " +
                                  Settings.Instance.nextTurnDeep + " " + Settings.Instance.nextTurnMaxWide + " " +
                                  Settings.Instance.nextTurnTotalBoards;
            }

            if (Settings.Instance.playaround)
            {
                this.botbehave += " playaround";
                this.botbehave += " " + Settings.Instance.playaroundprob + " " + Settings.Instance.playaroundprob2;
            }

            this.botbehave += " ets " + Settings.Instance.enemyTurnMaxWide;

            if (Settings.Instance.twotsamount > 0)
            {
                this.botbehave += " ets2 " + Settings.Instance.enemyTurnMaxWideSecondStep;
            }

            if (Settings.Instance.useSecretsPlayAround)
            {
                this.botbehave += " secret";
            }

            if (Settings.Instance.secondweight != 0.5f)
            {
                this.botbehave += " weight " + (int) (Settings.Instance.secondweight*100f);
            }

            if (Settings.Instance.placement != 0)
            {
                this.botbehave += " plcmnt:" + Settings.Instance.placement;
            }

            this.botbehave += " aA " + Settings.Instance.adjustActions;
            if (Settings.Instance.concedeMode != 0) this.botbehave += " cede:" + Settings.Instance.concedeMode;
            
        }
        
        public void updateCThunInfo(int OgOwnCThunAngrBonus, int OgOwnCThunHpBonus, int OgOwnCThunTaunt)
        {
            this.anzOgOwnCThunAngrBonus = OgOwnCThunAngrBonus;
            this.anzOgOwnCThunHpBonus = OgOwnCThunHpBonus;
            this.anzOgOwnCThunTaunt = OgOwnCThunTaunt;
            Hrtprozis.Instance.updateCThunInfo(this.anzOgOwnCThunAngrBonus, this.anzOgOwnCThunHpBonus, this.anzOgOwnCThunTaunt);
        }

        public void updateCThunInfobyCThun()
        {
            bool found = false;
            foreach (Handmanager.Handcard hc in this.handCards)
            {
                if (hc.card.name == CardDB.cardName.cthun)
                {
                    this.anzOgOwnCThunAngrBonus = hc.addattack;
                    this.anzOgOwnCThunHpBonus = hc.addHp;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                foreach (Minion m in this.ownMinions)
                {
                    if (m.name == CardDB.cardName.cthun)
                    {
                        if (this.anzOgOwnCThunAngrBonus < m.Angr - 6) this.anzOgOwnCThunAngrBonus = m.Angr - 6;
                        if (this.anzOgOwnCThunHpBonus < m.Hp - 6) this.anzOgOwnCThunHpBonus = m.Angr - 6;
                        if (m.taunt && this.anzOgOwnCThunTaunt < 1) this.anzOgOwnCThunTaunt++;
                        found = true;
                        break;
                    }
                }
            }
        }

        public static int getLastAffected(int entityid)
        {

            List<HSCard> allEntitys = TritonHs.GetAllCards();

            foreach (HSCard ent in allEntitys)
            {
                if (ent.GetTag(GAME_TAG.LAST_AFFECTED_BY) == entityid) return ent.GetTag(GAME_TAG.ENTITY_ID);
            }

            return 0;
        }

        public static int getCardTarget(int entityid)
        {

            List<HSCard> allEntitys = TritonHs.GetAllCards();

            foreach (HSCard ent in allEntitys)
            {
                if (ent.GetTag(GAME_TAG.ENTITY_ID) == entityid) return ent.GetTag(GAME_TAG.CARD_TARGET);
            }

            return 0;

        }


        private void printstuff()  //会输出到文件夹UltimateLogs里面
        {
            Playfield p = this.lastpf;
            string dtimes = DateTime.Now.ToString("HH:mm:ss:ffff");
            // 记录对局信息
            printUtils.printRecord = true;
            printUtils.recordRounds = string.Format("{0}法力水晶 第{1}回合 第{2}步操作.txt", p.ownMaxMana, gTurn, gTurnStep ); 
            string enemysecretIds = "";
            enemysecretIds = Probabilitymaker.Instance.getEnemySecretData();
            Helpfunctions.Instance.logg("#######################################################################");
            Helpfunctions.Instance.logg("#######################################################################");
            Helpfunctions.Instance.logg("开始计算, 已花费时间: " + DateTime.Now.ToString("HH:mm:ss") + " V" +
                                        this.versionnumber + " " + this.botbehave);
            
            // 输出当前场面价值判定
            string normalInfo = "";
            String enemyVal = "[敌方场面] ";
            String myVal = "[我方场面] ";
            String handCard = "[我方手牌] ";

            normalInfo += "水晶： " + p.mana + " / " + p.ownMaxMana
                + " [我方英雄] " + p.ownHeroName + " （生命: " + p.ownHero.Hp + " + " + p.ownHero.armor 
                + (p.ownWeapon != null ? " 武器: " + p.ownWeapon.card.chnName + " ( " + p.ownWeapon.Angr + " / " + p.ownWeapon.Durability + " ) " : "") 
                + (p.ownSecretsIDList.Count > 0 ? " 奥秘数: " + p.ownSecretsIDList.Count : "") + ") "
                + "[敌方英雄] " + p.enemyHeroName + " （生命: " + p.enemyHero.Hp + " + " + p.enemyHero.armor
                + (p.enemyWeapon != null ? " 武器: " + p.enemyWeapon.card.chnName + " ( " + p.enemyWeapon.Angr + " / " + p.enemyWeapon.Durability + " ) " : "")
                + (p.enemySecretCount > 0 ? " 奥秘数: " + p.enemySecretCount : "") + (p.enemyHero.immune ? " 免疫" : "") + ") ";
            foreach (Minion m in p.enemyMinions)
            {
                enemyVal += m.handcard.card.chnName + " ( " + m.Angr + " / " + m.Hp + " )" + (m.frozen ? "[冻结]" : "") + (m.cantAttack ? "[无法攻击]" : "") + (m.windfury ? "[风怒]" : "") + (m.taunt ? "[嘲讽]" : " ");
            }
            foreach (Minion m in p.ownMinions)
            {
                myVal += m.handcard.card.chnName + " ( " + m.Angr + " / " + m.Hp + " )" + (m.frozen ? "[冻结]" : "") + (!m.Ready || m.cantAttack ? "[无法攻击]" : "") + (m.windfury ? "[风怒]" : "") + (m.taunt ? "[嘲讽]" : " ");
            }
            foreach (Handmanager.Handcard hc in p.owncards)
            {
                handCard += hc.card.chnName + "(费用： " + hc.manacost + " ； " + (hc.addattack + hc.card.Attack) + " / " + (hc.addHp + hc.card.Health) + " ) ";
            }
            Helpfunctions.Instance.logg(normalInfo);
            Helpfunctions.Instance.logg(enemyVal);
            Helpfunctions.Instance.logg(myVal);
            Helpfunctions.Instance.logg(handCard);

            Helpfunctions.Instance.logg("#######################################################################");
            Helpfunctions.Instance.logg("#######################################################################");
            Helpfunctions.Instance.logg("turn " + gTurn + "/" + gTurnStep);// gTurn：如果先手，则1357... 如果后手 2468
            //gTurnStep是该回合，多次出牌/动作的每一步
            Helpfunctions.Instance.logg("mana " + currentMana + "/" + ownMaxMana);
            Helpfunctions.Instance.logg("emana " + enemyMaxMana);
            Helpfunctions.Instance.logg("own secretsCount: " + ownSecretList.Count);
            Helpfunctions.Instance.logg("enemy secretsCount: " + enemySecretList.Count + " ;" + enemysecretIds);

            Ai.Instance.currentCalculatedBoard = dtimes;

            Hrtprozis.Instance.printHero();
            Hrtprozis.Instance.printOwnMinions();
            Hrtprozis.Instance.printEnemyMinions();
            Handmanager.Instance.printcards();
            Probabilitymaker.Instance.printTurnGraveYard();
            Probabilitymaker.Instance.printGraveyards();
            p.prozis.printOwnDeck();
            //Hrtprozis.Instance.printOwnDeck();
            printUtils.printRecord = false;
        }

    }
}


