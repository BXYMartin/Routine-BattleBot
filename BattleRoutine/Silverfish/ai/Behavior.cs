﻿
using System.Collections.Generic;

namespace HREngine.Bots
{
    public abstract class Behavior
    {
        /// <summary>
        /// 场面值通用
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public virtual float getGeneralVal(Playfield p)
        {
            // 场面值
            int retval = 0;

            // 斩杀了
            if (p.enemyHero.Hp <= 0)
            {
                retval += 10000;
            }
            
            // --------------------------通用---------------------------------
            // 计算额外惩罚
            retval -= p.evaluatePenality;
            // 手牌价值（可能导致不出牌）
            // retval += p.owncards.Count * 5;
            // 最大法力水晶
            retval += p.ownMaxMana * 20 - p.enemyMaxMana * 20;
            // 英雄技能
            // retval += (p.enemyHeroAblility.manacost - p.ownHeroAblility.manacost) * 4;
            // if (p.ownHeroPowerAllowedQuantity != p.enemyHeroPowerAllowedQuantity)
            // {
            //     if(p.ownHeroPowerAllowedQuantity > p.enemyHeroPowerAllowedQuantity) retval += 3;
            //     else retval -= 3;
            // }
            // 对法师，需要考虑法强效果
            //if (p.enemyHeroName == HeroEnum.mage) retval -= 4 * p.enemyspellpower;
            // 武器价值
            //if (p.ownWeapon.Angr > 0)
            //{
            //    if (p.ownWeapon.Angr > 1) retval += p.ownWeapon.Angr * p.ownWeapon.Durability * 2;
            //    else retval += (p.ownWeapon.Angr + 1) * p.ownWeapon.Durability * 2 - 3;
            //}
            // 敌方武器价值
            //if (!p.enemyHero.frozen)
            //{
            //    retval -= p.enemyWeapon.Durability * p.enemyWeapon.Angr;
            //}
            //else
            //{
            //    if (p.enemyWeapon.Durability >= 1)
            //    {
            //        retval += 12;
            //    }
            //}
            // 抽牌价值
            //RR card draw value depending on the turn and distance to lethal
            //RR if lethal is close, carddraw value is increased
            if (p.lethalMissing() <= 5) //RR
            {
                retval += p.owncarddraw * 100;
            }
            if (p.ownMaxMana < 4)
            {
                retval += p.owncarddraw * 2;
            }
            else
            {
                retval += p.owncarddraw * 5;
            }
            // 卡差
            // if (p.owncarddraw + 1 >= p.enemycarddraw) retval -= p.enemycarddraw * 7;
            // else retval -= (p.owncarddraw + 1) * 7 + (p.enemycarddraw - p.owncarddraw - 1) * 12;
            // 计算我方随从价值
            int owntaunt = 0;
            // int readycount = 0;
            int ownMinionsCount = 0;
            foreach (Minion m in p.ownMinions)
            {
                retval += getMyMinionValue(m, p);
                if (m.Hp <= 4 && (m.Angr > 2 || m.Hp > 3)) ownMinionsCount++;
                if (m.taunt) owntaunt++;
                retval += m.synergy;
            }
            // 危险血量
            if (p.ownHero.Hp + p.ownHero.armor < 12)
            {
                if (owntaunt > 0)
                {
                    retval += 20;
                }
            }
            // 克苏恩计数器
            // retval += p.anzOgOwnCThunAngrBonus;
            retval += p.anzOwnExtraAngrHp - p.anzEnemyExtraAngrHp;

            /*if (p.enemyMinions.Count >= 0)
            {
                int anz = p.enemyMinions.Count;
                if (owntaunt == 0) retval -= 10 * anz;
                retval += owntaunt * 10 - 11 * anz;
            }*/

            // 使用英雄技能
            bool useAbili = false;
            // 使用硬币
            int usecoin = 0;
            //soulfire etc
            // int deletecardsAtLast = 0;
            int wasCombo = 0;
            bool firstSpellToEnHero = false;
            // 出牌序列数量
            int count = p.playactions.Count;
            int ownActCount = 0;
            // 排序问题！！！！
            for (int i = 0; i < count; i++)
            {
                Action a = p.playactions[i];
                ownActCount++;
                switch (a.actionType)
                {
                    // 出售随从
                    case actionEnum.sellMinion:
                        usecoin++;
                        retval -= getEnemyMinionValue(a.own, p);
                        continue;
                    case actionEnum.buyMinion:
                        usecoin -= 3;
                        retval += getEnemyMinionValue(a.own, p);
                        continue;
                    case actionEnum.useHeroPower:
                        useAbili = true;
                        continue;
                    case actionEnum.playcard:
                        break;
                    default:
                        continue;
                }
                switch (a.card.card.chnName)
                {
                    case "幸运币":
                        usecoin++;
                        // 最后一张卡
                        if (i == count - 1) retval -= 10;
                        break;
                }
                // 连击
                //if (a.card.card.Combo && i > 0) wasCombo++;
                //if (a.target == null) continue;
                //save spell for all classes
                // 法术不打脸？
                if (a.card.card.type == CardDB.cardtype.SPELL && (a.target.isHero && !a.target.own))
                {
                    if (i == 0) firstSpellToEnHero = true;
                    retval -= 11;
                }
            }
            if (wasCombo > 0 && firstSpellToEnHero)
            {
                if (wasCombo + 1 == ownActCount) retval += 10;
            }
            if (usecoin > 0)
            {
                retval -= 10 * p.manaTurnEnd;
                if (p.manaTurnEnd + usecoin > 10) retval -= 10 * usecoin;
            }
            // 法力水晶还剩下 2 个并且还能用英雄技能
            // Todo: 英雄技能更新
            if (p.manaTurnEnd >= p.ownHeroAblility.manacost && !useAbili)
            {
                switch (p.ownHeroAblility.card.name)
                {
                    case CardDB.cardName.heal: goto case CardDB.cardName.lesserheal;
                    case CardDB.cardName.lesserheal:
                        bool wereTarget = false;
                        if (p.ownHero.Hp < p.ownHero.maxHp) wereTarget = true;
                        if (!wereTarget)
                        {
                            foreach (Minion m in p.ownMinions)
                            {
                                if (m.wounded) { wereTarget = true; break; }
                            }
                        }
                        if (wereTarget && !(p.anzOwnAuchenaiSoulpriest > 0 || p.embracetheshadow > 0)) retval -= 10;
                        break;
                    case CardDB.cardName.poisoneddaggers: goto case CardDB.cardName.daggermastery;
                    case CardDB.cardName.daggermastery:
                        if (!(p.ownWeapon.Durability > 1 || p.ownWeapon.Angr > 1)) retval -= 10;
                        break;
                    case CardDB.cardName.totemicslam: goto case CardDB.cardName.totemiccall;
                    case CardDB.cardName.totemiccall:
                        if (p.ownMinions.Count < 7) retval -= 10;
                        else retval -= 3;
                        break;
                    case CardDB.cardName.thetidalhand: goto case CardDB.cardName.reinforce;
                    case CardDB.cardName.thesilverhand: goto case CardDB.cardName.reinforce;
                    case CardDB.cardName.reinforce:
                        if (p.ownMinions.Count < 7) retval -= 10;
                        else retval -= 3;
                        break;
                    case CardDB.cardName.soultap:
                        if (p.owncards.Count < 10 && p.ownDeckSize > 0)
                        {
                            retval -= 20;
                        }
                        break;
                    case CardDB.cardName.lifetap:
                        if (p.owncards.Count < 10 && p.ownDeckSize > 0)
                        {
                            if (p.ownHero.Hp > 10)
                                retval -= 20;
                            else if (p.ownHero.Hp > 2)
                                retval -= 10;
                        }
                        break;
                    default:
                        retval -= 30;
                        break;
                }
            }
            // if (usecoin && p.mana >= 1) retval -= 20;
            // 手里的随从
            int mobsInHand = 0;
            int bigMobsInHand = 0;
            foreach (Handmanager.Handcard hc in p.owncards)
            {
                if (hc.card.type == CardDB.cardtype.MOB)
                {
                    mobsInHand++;
                    if (hc.card.Attack + hc.addattack >= 3) bigMobsInHand++;
                    retval += hc.addattack + hc.addHp + hc.elemPoweredUp;
                }
            }

            if (ownMinionsCount - p.enemyMinions.Count >= 4 && bigMobsInHand >= 1)
            {
                retval += bigMobsInHand * 25;
            }

            // 敌方随从
            //bool hasTank = false;
            // 敌方已死，不考虑随从加权
            //if(p.enemyHero.Hp > 0)
            //{
            //    foreach (Minion m in p.enemyMinions)
            //    {
            //        retval -= this.getEnemyMinionValue(m, p);
            //        //hasTank = hasTank || m.taunt;
            //    }
            //}            
            //retval -= p.enemyMinions.Count * 2;
            // 敌方奥秘
            //retval -= p.enemySecretCount;
            // 溢出伤害
            //retval -= p.lostDamage;//damage which was to high (like killing a 2/1 with an 3/3 -> => lostdamage =2
            // 溢出武器伤害
            //retval -= p.lostWeaponDamage;

            //if (p.ownMinions.Count == 0) retval -= 20;
            //if (p.enemyMinions.Count == 0) retval += 20;
            // 已斩杀
            //if (p.enemyHero.Hp <= 0)
            //{
            //    retval += 10000;
            //    if (retval < 10000) retval = 10000;
            //}
            // 感觉要死
            if (p.enemyHero.Hp >= 1 && p.guessingHeroHP <= 0)
            {
                retval -= 1000;
            }
            // 濒死
            if (p.ownHero.Hp <= 0) retval -= 20000;

            //retval += getCantAcceptPenality(p);

            return retval;
        }

        /// <summary>
        /// 场面值，决定AI如何判定最优解
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public virtual float getPlayfieldValue(Playfield p)
        {
            return 0;
        }

        /// <summary>
        /// 敌我生命值的价值判定
        /// </summary>
        /// <param name="p">场面</param>
        /// <param name="hpboarder">我方危险血线</param>
        /// <param name="aggroboarder">敌方危险血线</param>
        /// <returns></returns>
        public virtual int getHpValue(Playfield p, int hpboarder, int aggroboarder)
        {
            int retval = 0;
            // 血线安全
            if (p.ownHero.Hp + p.ownHero.armor > hpboarder)
            {
                retval += (2 + p.ownHero.Hp + p.ownHero.armor - hpboarder ) / 2;
            }
            // 快死了
            else
            {
                if (p.nextTurnWin()) retval -= (hpboarder + 1 - p.ownHero.Hp - p.ownHero.armor);
                else retval -= 2 * (hpboarder + 1 - p.ownHero.Hp - p.ownHero.armor) * (hpboarder + 1 - p.ownHero.Hp - p.ownHero.armor);
            }
            /*
            if (p.enemyHero.Hp + p.enemyHero.armor > aggroboarder)
            {
                retval += 3 - p.enemyHero.Hp - p.enemyHero.armor + aggroboarder;
            }
            else if (p.enemyHero.Hp + p.enemyHero.armor <= aggroboarder && p.enemyHero.Hp + p.enemyHero.armor > aggroboarder / 3)
            {
                retval += 3 * (aggroboarder + 1 - p.enemyHero.Hp - p.enemyHero.armor);
            }
            else
            {
                retval += 4 * (aggroboarder + 1 - p.enemyHero.Hp - p.enemyHero.armor);
            }
            */
            return retval;
        }


        public virtual int getEnemyMinionValue(Minion m, Playfield p)
        {
            return 0;
        }

        public virtual string BehaviorName()
        {
            return "None";
        }

        public virtual int getPlayCardPenality(CardDB.Card card, Minion target, Playfield p)
        {
            return 0;
        }

        public virtual int getAttackWithHeroPenality(Minion target, Playfield p)
        {
            return 0;
        }

        public virtual int getAttackWithMininonPenality(Minion m, Playfield p, Minion target)
        {
            return 0;
        }

        public virtual int getSirFinleyPriority(List<Handmanager.Handcard> discoverCards)
        {
            return -1;
        }

        public virtual int getSirFinleyPriority(CardDB.Card card)
        {
            return -1;
        }



        public virtual int getComboPenality(CardDB.Card card, Minion target, Playfield p)  
            // 每个策略写自己特定的，通用的放PenalityManager里面getSpecial...，所以要注意不要写重复了，避免双重惩罚
        {
            return 0;
        }

        public virtual int getMyMinionValue(Minion m, Playfield p)
        {
            int retval = 5;
            retval += m.Hp * 2;
            if(!m.cantAttack || !m.Ready || !m.frozen){
                retval += m.Angr * 2;
            }else {
                retval += m.Angr / 2;
            }
            // 风怒价值
            if ((!m.playedThisTurn || m.rush == 1 || m.charge == 1 )  && m.windfury) retval += m.Angr;
            // 圣盾价值
            if (m.divineshild) retval += m.Angr / 2 + 1;
            // 潜行价值
            if (m.stealth) retval += m.Angr / 3 + 1;
            // 吸血
            if (m.lifesteal) retval += m.Angr / 3 + 1;
            // 圣盾嘲讽
            if (m.divineshild && m.taunt) retval += 4;
            retval += m.synergy;
            return retval;
        }
        
        // 英雄受到伤害
        public virtual int getSecretPen_HeroGotDmg(Playfield p, SecretItem si, bool own, int dmg)
        {
            int pen = 0;
            // 冰箱，尽量打到 1 血
            if (si.canBe_iceblock)
            {
                // 破冰时刻
                if(p.enemyHero.Hp - dmg < 0 && p.enemyHero.Hp > 0)
                {
                    pen -= 100 / (p.enemyHero.Hp);
                }
            }
            return pen;
        }

        // 打出随从
        public virtual int getSecretPen_MinionIsPlayed(Playfield p, SecretItem si, Minion playedMinion)
        {
            int pen = 0;
            // 打出随从的基本惩罚
            if (si.canBe_snipe)
            {
                // 3费及以下可以接受
                pen -= 9;
                pen += playedMinion.handcard.card.cost * 3;
                // 冲锋有可能冲不出哦
                if (playedMinion.handcard.card.Charge && (playedMinion.handcard.addHp+ playedMinion.handcard.card.Health ) <= 4 ) 
                    pen += 30;
                // 亡语抵消
                if (playedMinion.handcard.card.deathrattle)
                {
                    pen -= 6;
                }
            }
            return pen;
        }

        // 打出法术
        public virtual int getSecretPen_SpellIsPlayed(Playfield p, SecretItem si, Minion target, CardDB.Card c)
        {
            if(c.type != CardDB.cardtype.SPELL)
            {
                return 0;
            }
            int pen = 0;
            // 打出法术基本惩罚
            if (si.canBe_counterspell)
            {
                // 2费不亏
                pen -= 10;
                pen += c.cost * 5;
                // 硬币破法反，喜闻乐见喜闻乐见
                if (c.cost == 0 && p.ownMaxMana >= 3) pen -= 100;
            }
            return pen;
        }

        /// <summary>
        /// 随从死亡
        /// </summary>
        /// <param name="si"></param>
        /// <param name="own"></param>
        /// <returns></returns>
        public virtual int getSecretPen_MinionDied(Playfield p, SecretItem si, bool own)
        {
            int pen = 0;
            return pen;
        }

        // 英雄技能
        public virtual int getSecretPen_HeroPowerUsed(Playfield p, SecretItem si)
        {
            int pen = 0;
            return pen;
        }

        public virtual int getDiscoverVal(CardDB.Card card, Playfield p)
        {
            return 0;
        }
    }

}