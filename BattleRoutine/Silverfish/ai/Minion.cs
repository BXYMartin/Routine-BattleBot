﻿using Triton.Game.Mapping;

namespace HREngine.Bots
{
    using System;
    using System.Collections.Generic;

    public class miniEnch
    {
        public CardDB.cardIDEnum CARDID = CardDB.cardIDEnum.None;
        public int creator = 0; // the minion
        public int controllerOfCreator = 0; // own or enemys buff?
        //public int copyDeathrattle = 0;

        public miniEnch(CardDB.cardIDEnum id, int crtr, int controler)
        {
            this.CARDID = id;
            this.creator = crtr;
            this.controllerOfCreator = controler;
            //this.copyDeathrattle = copydr;
        }

    }

    public class Minion
    {
        //dont silence----------------------------
        public int anzGotDmg = 0;//受到伤害次数
        public int GotDmgValue = 0;//受到伤害总和
        public int anzGotHealed = 0;//受到治疗次数
        public int GotHealedValue = 0;//受到治疗总和
        public bool gotInspire = false;//得到激励
        public bool isHero = false;//是否为英雄
        public bool own; //是否为己方
        public int pID = 0; //PID

        public CardDB.cardName name = CardDB.cardName.unknown;//随从名称
        public TAG_CLASS cardClass = TAG_CLASS.INVALID;//随从类别
        public TAG_RACE cardRace = TAG_RACE.INVALID;//随从种族
        public int synergy = 0;//职业契合度，即各种族（机械鱼人恶魔野兽）与职业相关性
        public Handmanager.Handcard handcard;
        //手牌信息，如果Minion中储存的内容不够的话，可以调用这个获取更多的信息，例如 m.handcard.card
        public int entitiyID = -1;//实例ID
        //public int id = -1;//delete this
        public int zonepos = 0;//随从放置位置
        public CardDB.Card deathrattle2;//亡语2号，比如其他随从的亡语技能转移

        public bool playedThisTurn = false;//在这回合使用
        public bool playedPrevTurn = false;//在上回合使用
        
        public bool cantAttackHeroes = false;//无法攻击英雄
        public bool cantAttack = false;//无法攻击

        public int Hp = 0;//当前血量
        public int maxHp = 0;//最大血量
        public int armor = 0;//护甲值（英雄

        public int Angr = 0;//攻击力
        public int AdjacentAngr = 0;//相邻buff攻击力加成
        public int tempAttack = 0;//临时攻击力
        public int justBuffed = 0;//仅仅buff

        public bool Ready = false;//攻击准备就绪

        public bool taunt = false;//嘲讽
        public bool wounded = false;//hp red?//受伤

        public bool frenzy = false;//暴怒
        public bool divineshild = false;//圣盾
        public bool windfury = false; //风怒
        public bool frozen = false;//冻结
        public bool stealth = false;//隐身
        public bool immune = false;//免疫
        public bool untouchable = false;//无法被攻击
        public bool exhausted = false;//用尽的   法力值?
        public bool lifesteal = false;//吸血
        public int dormant = 0;//休眠
        public bool outcast = false;//流放
		public bool reborn = false;//复生
       
        //法术迸发
        public bool Spellburst
        {
            get { return _spellburst; }
            set { _spellburst = value; }
        }
        private bool _spellburst = false;

        //public bool modular = false; //磁力
        public int charge = 0;//冲锋
        public int rush = 0;//突袭
        public int hChoice = 0;
        public bool poisonous = false;//剧毒
        public bool cantLowerHPbelowONE = false;//血量无法低于1
        
        public bool playedFromHand = false;//从手牌中打出
        public bool extraParam = false;//扩展参数1
        public int extraParam2 = 0;//扩展参数2

        public Minion()
        {
            this.handcard = new Handmanager.Handcard();
        }

        public Minion(Minion m)
        {
            //dont silence----------------------------
            //this.anzGotDmg = m.anzGotDmg;
            //this.GotDmgValue = m.GotDmgValue;
            //this.anzGotHealed = m.anzGotHealed;
            this.gotInspire = m.gotInspire;
            this.isHero = m.isHero;
            this.own = m.own;

            this.name = m.name;
            this.cardClass = m.cardClass;
            this.cardRace = m.cardRace;
            this.synergy = m.synergy;
            this.handcard = m.handcard;
            this.deathrattle2 = m.deathrattle2;
            this.entitiyID = m.entitiyID;
            this.zonepos = m.zonepos;

            this.playedThisTurn = m.playedThisTurn;
            this.playedPrevTurn = m.playedPrevTurn;
            
            this.Hp = m.Hp;
            this.maxHp = m.maxHp;
            this.armor = m.armor;

            this.Angr = m.Angr;
            this.AdjacentAngr = m.AdjacentAngr;
            this.tempAttack = m.tempAttack;
            this.justBuffed = m.justBuffed;

            this.Ready = m.Ready;

            this.taunt = m.taunt;
            this.wounded = m.wounded;

            this.frenzy = m.frenzy;
            this.divineshild = m.divineshild;
            this.windfury = m.windfury;
            this.frozen = m.frozen;
            this.stealth = m.stealth;
            this.immune = m.immune;
            this.untouchable = m.untouchable;
            this.exhausted = m.exhausted;

            this.Spellburst = m.Spellburst;//法术迸发
            this.charge = m.charge;
            this.rush = m.rush;
            this.hChoice = m.hChoice;
            this.poisonous = m.poisonous;
            this.lifesteal = m.lifesteal;
            this.dormant = m.dormant;
            this.outcast = m.outcast;
			this.reborn = m.reborn;
            this.cantLowerHPbelowONE = m.cantLowerHPbelowONE;
            
            this.cantAttackHeroes = m.cantAttackHeroes;
            this.cantAttack = m.cantAttack;
        }

        public void setMinionToMinion(Minion m)
        {
            //dont silence----------------------------
            this.anzGotDmg = m.anzGotDmg;
            this.GotDmgValue = m.GotDmgValue;
			this.anzGotHealed = m.anzGotHealed;
            this.gotInspire = m.gotInspire;
            this.isHero = m.isHero;
            this.own = m.own;

            this.name = m.name;
            this.cardRace = m.cardRace;
            this.cardClass = m.cardClass;
            this.synergy = m.synergy;
            this.handcard = m.handcard;
            this.deathrattle2 = m.deathrattle2;

            this.zonepos = m.zonepos;
            
            this.Hp = m.Hp;
            this.maxHp = m.maxHp;
            this.armor = m.armor;

            this.Angr = m.Angr;
            this.AdjacentAngr = m.AdjacentAngr;
            this.tempAttack = m.tempAttack;

            this.frenzy = m.frenzy;
            this.taunt = m.taunt;
            this.wounded = m.wounded;

            this.divineshild = m.divineshild;
            this.windfury = m.windfury;
            this.frozen = m.frozen;
            this.stealth = m.stealth;
            this.immune = m.immune;
            this.untouchable = m.untouchable;
            this.exhausted = m.exhausted;

            this.Spellburst = m.Spellburst;//法术迸发
            this.charge = m.charge;
            this.rush = m.rush;
            this.hChoice = m.hChoice;
            this.Ready = false;
            if (m.rush > 0 && m.playedThisTurn) this.cantAttackHeroes = true;
            this.poisonous = m.poisonous;
            this.lifesteal = m.lifesteal;
            this.dormant = m.dormant;
            this.outcast = m.outcast;
			this.reborn = m.reborn;
            this.cantLowerHPbelowONE = m.cantLowerHPbelowONE;
            
            this.cantAttackHeroes = m.cantAttackHeroes;
            this.cantAttack = m.cantAttack;
        }

        public int getRealAttack()
        {
            return this.Angr;
        }

        //受到伤害
        public void getDamageOrHeal(int dmg, Playfield p, bool isMinionAttack, bool dontCalcLostDmg)
        {
            if (this.Hp <= 0) return;

            if (this.immune && dmg > 0 || this.untouchable) return;
            
            int damage = dmg;
            int heal = 0;
            if (dmg < 0) heal = -dmg;

            if (this.isHero)
            {
                if (this.own)
                {
                    if (p.ownWeapon.name == CardDB.cardName.cursedblade) dmg += dmg;
                    if (p.anzOwnAnimatedArmor > 0 && dmg > 0) dmg = 1;
                    if (p.anzOwnBolfRamshield > 0 && dmg > 0)


                    {
                        int rest = this.armor - dmg;
                        this.armor = Math.Max(0, rest);
                        if (rest < 0)
                        {
                            foreach (Minion m in p.ownMinions)
                            {
                                if (m.name == CardDB.cardName.bolframshield)
                                {
                                    m.getDamageOrHeal(-rest, p, true, false);
                                    break;
                                }
                            }
                        }
                        return;
                    }
                }
                else
                {
                    if (p.anzEnemyAnimatedArmor > 0 && dmg > 0) dmg = 1;
                    if (p.enemyWeapon.name == CardDB.cardName.cursedblade) dmg += dmg;
                    if (p.anzEnemyBolfRamshield > 0 && dmg > 0)
                    {
                        int rest = this.armor - dmg;
                        this.armor = Math.Max(0, rest);
                        if (rest < 0)
                        {
                            foreach (Minion m in p.enemyMinions)
                            {
                                if (m.name == CardDB.cardName.bolframshield)
                                {
                                    m.getDamageOrHeal(-rest, p, true, false);
                                    break;
                                }
                            }
                        }
                        return;
                    }
                }

                int copy = this.Hp;
                if (heal > 0)
                {
                    this.Hp = Math.Min(this.maxHp, this.Hp + heal);
                    if (copy < this.Hp)
                    {
                        p.tempTrigger.charsGotHealed++;
                        this.anzGotHealed++;
                        this.GotHealedValue += heal;
                    }
                }
                else if (dmg > 0)
                {
                    int rest = this.armor - dmg;
                    if (rest < 0) this.Hp += rest;
                    this.armor = Math.Max(0, this.armor - dmg);


                    if (this.cantLowerHPbelowONE && this.Hp <= 0) this.Hp = 1;
                    if (copy > this.Hp)
                    {
                        this.anzGotDmg++;
                        this.GotDmgValue += dmg;
                        if (this.own)
                        {
                            p.tempTrigger.ownMinionsGotDmg++;
                            p.tempTrigger.ownHeroGotDmg++;
                        }
                        else
                        {
                            p.tempTrigger.enemyMinionsGotDmg++;
                            p.tempTrigger.enemyHeroGotDmg++;
                        }
                    }
                }
                return;
            }

            //its a Minion--------------------------------------------------------------
            
            bool woundedbefore = this.wounded;

            if (damage > 0 && this.divineshild)
            {
                p.minionLosesDivineShield(this);
                if (!own && !dontCalcLostDmg && p.turnCounter == 0)
                {
                    if (isMinionAttack)
                    {
                        p.lostDamage += damage - 1;
                    }
                    else
                    {
                        p.lostDamage += (damage - 1) * (damage - 1);
                    }
                }
                return;
            }

            if (this.cantLowerHPbelowONE && damage >= 1 && damage >= this.Hp) damage = this.Hp - 1;

            if (!own && !dontCalcLostDmg && this.Hp < damage && p.turnCounter == 0)
            {
                if (isMinionAttack)
                {
                    p.lostDamage += (damage - this.Hp);
                }
                else
                {
                    p.lostDamage += (damage - this.Hp) * (damage - this.Hp);
                }
            }

            int hpcopy = this.Hp;

            if (damage >= 1)
            {
                this.Hp = this.Hp - damage;
            }

            if (heal >= 1)
            {
                if (own && !dontCalcLostDmg && heal <= 999 && this.Hp + heal > this.maxHp) p.lostHeal += this.Hp + heal - this.maxHp;

                this.Hp = this.Hp + Math.Min(heal, this.maxHp - this.Hp);
            }



            if (this.Hp > hpcopy)
            {
                //minionWasHealed
                p.tempTrigger.minionsGotHealed++;
                p.tempTrigger.charsGotHealed++;
                this.anzGotHealed++;
                this.GotHealedValue += heal;
            }
            else if (this.Hp < hpcopy)
            {
                if (this.own) p.tempTrigger.ownMinionsGotDmg++;
                else p.tempTrigger.enemyMinionsGotDmg++;

                if (p.anzAcidmaw > 0)
                {
                    if (p.anzAcidmaw == 1)
                    {
                        if (this.name != CardDB.cardName.acidmaw) this.Hp = 0;
                    }
                    else this.Hp = 0;
                }

                this.anzGotDmg++;
                this.GotDmgValue += dmg;
            }

            if (this.maxHp == this.Hp)
            {
                this.wounded = false;
            }
            else
            {
                this.wounded = true;
            }
            
            if (woundedbefore && !this.wounded)
            {
                this.handcard.card.sim_card.onEnrageStop(p, this);
            }

            if (!woundedbefore && this.wounded)
            {
                this.handcard.card.sim_card.onEnrageStart(p, this);
            }
            
            if (this.Hp <= 0)
            {
                this.minionDied(p);
            }

        }
        //随从死亡
        public void minionDied(Playfield p)
        {
            // 复生
            if (this.reborn)
            {
                this.reborn = false;
                this.Hp = 1;
                // 具有突袭
                if (this.rush > 0)
                {
                    this.Ready = true;
                    this.cantAttackHeroes = true;
                    this.tempAttack = 0;
                }
                if (this.charge > 0)
                {
                    this.Ready = true;
                    this.tempAttack = 0;
                }
            }
            if (this.name == CardDB.cardName.stalagg)
            {
                p.stalaggDead = true;
            }
            else
            {
                if (this.name == CardDB.cardName.feugen) p.feugenDead = true;
            }

            

            if (own)
            {
                p.tempTrigger.ownMinionsDied++;
                if (this.taunt) p.anzOwnTaunt--;
                if (this.handcard.card.race == 20)
                {
                    p.tempTrigger.ownBeastDied++;
                }
                else if (this.handcard.card.race == 17)
                {
                    p.tempTrigger.ownMechanicDied++;
                }
                else if (this.handcard.card.race == 14)
                {
                    p.tempTrigger.ownMurlocDied++;
                }
            }
            else
            {
                p.tempTrigger.enemyMinionsDied++;
                if (this.taunt) p.anzEnemyTaunt--;
                if (this.handcard.card.race == 20)
                {
                    p.tempTrigger.enemyBeastDied++;
                }
                else if (this.handcard.card.race == 17)
                {
                    p.tempTrigger.enemyMechanicDied++;
                }
                else if (this.handcard.card.race == 14)
                {
                    p.tempTrigger.enemyMurlocDied++;
                }
            }

            if (p.diedMinions != null)
            {
                GraveYardItem gyi = new GraveYardItem(this.handcard.card.cardIDenum, this.entitiyID, this.own);
                p.diedMinions.Add(gyi);
            }
        }
        //更新状态
        public void updateReadyness()
        {
            Ready = false;
        }
        
        //攻击之后的状态
        public Minion GetTargetForMinionWithSurvival(Playfield p, bool own)
        {
            if (this.Angr == 0) return null;
            if ((own ? p.enemyMinions.Count : p.ownMinions.Count) < 1) return (own ? p.enemyHero : p.ownHero);
            Minion target = new Minion();
            Minion targetTaumt = new Minion();
            foreach (Minion m in own ? p.enemyMinions : p.ownMinions)
            {
                if (m.taunt)
                {
                    if (this.Hp > m.Hp && (m.Hp + m.Angr + m.Angr * (m.windfury ? 1 : 0)) > (targetTaumt.Hp + targetTaumt.Angr + targetTaumt.Angr * (targetTaumt.windfury ? 1 : 0))) targetTaumt = m;
                }
                else
                {
                    if (this.Hp > m.Hp && (m.Hp + m.Angr + m.Angr * (m.windfury ? 1 : 0)) > (target.Hp + target.Angr + target.Angr * (target.windfury ? 1 : 0))) target = m;
                }
            }
            if (targetTaumt.Hp > 0) return targetTaumt;
            if (target.Hp > 0) return target;
            return null;
        }
        //特殊随从的特效添加位置
        public void loadEnchantments(List<miniEnch> enchants, int ownPlayerControler)
        {
            foreach (miniEnch me in enchants)
            {
                // reborns and destoyings----------------------------------------------
                
            }
        }

        public string info() // 测试用，用于打印信息
        {
            if(isHero)
            {
                if(own)
                    return "己方英雄" + "("+ Hp +")";
                else
                    return "敌方英雄" + "(" + Hp + ")";
            }
            return handcard.card.chnName + "(" + Angr + "," + Hp + ")";
        }

    }

}