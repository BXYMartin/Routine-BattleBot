﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using log4net;
using Newtonsoft.Json;
using Triton.Bot.Settings;
using Triton.Common;
using Triton.Game.Mapping;
using Logger = Triton.Common.LogUtilities.Logger;

using Triton.Bot;
//using Triton.Common;
using Triton.Game;
using Triton.Game.Data;

namespace HREngine.Bots
{
    /// <summary>Settings for the DefaultRoutine. </summary>
    public class BattleRoutineSettings : JsonSettings
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        private static BattleRoutineSettings _instance;

        /// <summary>The current instance for this class. </summary>
        public static BattleRoutineSettings Instance
        {
            get { return _instance ?? (_instance = new BattleRoutineSettings()); }
        }

        /// <summary>The default ctor. Will use the settings path "DefaultRoutine".</summary>
        public BattleRoutineSettings()
            : base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "BattleRoutine")))
        {
            HeroList = new ObservableCollection<HandCardItem>();
            BaconShopList = new ObservableCollection<HandCardItem>();
            BattleGroundList = new ObservableCollection<HandCardItem>();
            HandCardList = new ObservableCollection<HandCardItem>();
        }

        private string _defaultBehavior;

        /// <summary>
        /// The first hero choice for arena if present.
        /// </summary>
        
        private ObservableCollection<TAG_CLASS> _allClasses;

        /// <summary>All enum values for this type.</summary>
        [JsonIgnore]
        public ObservableCollection<TAG_CLASS> AllClasses
        {
            get
            {
                return _allClasses ?? (_allClasses = new ObservableCollection<TAG_CLASS>
                {
                    TAG_CLASS.DRUID,
                    TAG_CLASS.HUNTER,
                    TAG_CLASS.MAGE,
                    TAG_CLASS.PALADIN,
                    TAG_CLASS.PRIEST,
                    TAG_CLASS.ROGUE,
                    TAG_CLASS.SHAMAN,
                    TAG_CLASS.WARLOCK,
                    TAG_CLASS.WARRIOR,
                });
            }
        }

        // Behavior choice.
        [DefaultValue("Control")]
        public string DefaultBehavior
        {
            get { return _defaultBehavior; }
            set
            {
                if (!value.Equals(_defaultBehavior))
                {
                    _defaultBehavior = value;
                    NotifyPropertyChanged(() => DefaultBehavior);
                }                
                Log.InfoFormat("[默认策略设置] 默认战斗模式 = {0}.", _defaultBehavior);                
            }
        }

        public ObservableCollection<HandCardItem> HeroList
        {
            get
            {
                return heroList;
            }
            set
            {
                heroList = value;
                NotifyPropertyChanged(() => HeroList);
            }
        }

        private ObservableCollection<HandCardItem> heroList;


        public ObservableCollection<HandCardItem> BaconShopList
        {
            get
            {
                return baconShopList;
            }
            set
            {
                baconShopList = value;
                NotifyPropertyChanged(() => BaconShopList);
            }
        }

        private ObservableCollection<HandCardItem> baconShopList;

        public ObservableCollection<HandCardItem> BattleGroundList
        {
            get
            {
                return battleGroundList;
            }
            set
            {
                battleGroundList = value;
                NotifyPropertyChanged(() => BattleGroundList);
            }
        }

        private ObservableCollection<HandCardItem> battleGroundList;

        public ObservableCollection<HandCardItem> HandCardList
        {
            get
            {
                return handCardList;
            }
            set
            {
                handCardList = value;
                NotifyPropertyChanged(() => HandCardList);
            }
        }

        private ObservableCollection<HandCardItem> handCardList;

        private ObservableCollection<string> _allBehav;

        /// <summary>All enum values for this type.</summary>
        [JsonIgnore]
        public ObservableCollection<string> AllBehav
        {
            get
            {
                return _allBehav ?? (_allBehav = new ObservableCollection<string>(Silverfish.Instance.BehaviorDB.Keys));
            }
        }



        // Emote choice.
        [DefaultValue("无")]
        public string DefaultEmote
        {
            get { return printUtils.emoteMode; }
            set
            {
                if (!value.Equals(printUtils.emoteMode))
                {
                    printUtils.emoteMode = value;
                    NotifyPropertyChanged(() => DefaultEmote);
                }
            }
        }

        private ObservableCollection<string> AllEmotes;

        /// <summary>All enum values for this type.</summary>
        [JsonIgnore]
        public ObservableCollection<string> AllEmote
        {
            get
            {
                return AllEmotes ?? (AllEmotes = new ObservableCollection<string>() { "无", "友善模式", "嘴臭模式", "乞讨模式", "摊牌了我是脚本", "精神污染模式", "抱歉" } );
            }
        }

        private readonly List<int> _questIdsToCancel = new List<int>();

		[JsonIgnore]
	    public List<int> QuestIdsToCancel
	    {
		    get { return _questIdsToCancel; }
	    }

        private int _maxWide;

        /// <summary>
        /// AI值.
        /// </summary>
        [DefaultValue(-1)]
        public int MaxWide
        {
            get { return _maxWide; }
            set
            {
                if (!value.Equals(_maxWide))
                {
                    _maxWide = value;
                    NotifyPropertyChanged(() => MaxWide);
  
                }
                Log.InfoFormat("[默认策略设置] AI值 = {0}.", _maxWide);
            }
        }

        
        /// <summary>
        /// 是否打印出牌惩罚.
        /// </summary>
        [DefaultValue(false)]
        public bool UsePrintNextMove
        {
            get { return printUtils.printNextMove; }
            set
            {
                if (!value.Equals(printUtils.printNextMove))
                {
                    printUtils.printNextMove = value;
                    NotifyPropertyChanged(() => UsePrintPenalties);
                }
                Log.InfoFormat("[默认策略设置] 打印出牌惩罚 = {0}.", printUtils.printNextMove);
            }
        }

        /// <summary>
        /// 是否打印出牌惩罚.
        /// </summary>
        [DefaultValue(false)]
        public bool UseClearStatus
        {
            get { return clearStatus; }
            set
            {
                if (!value.Equals(clearStatus))
                {
                    clearStatus = value;
                    NotifyPropertyChanged(() => UseClearStatus);
                }
                Log.InfoFormat("[默认策略设置] 清除每回合信息 = {0}.", clearStatus);
            }
        }

        private bool clearStatus = false;

        /// <summary>
        /// 是否打印自定义惩罚.
        /// </summary>
        [DefaultValue(false)]
        public bool UsePrintPenalties
        {
            get { return printUtils.printPentity; }
            set
            {
                if (!value.Equals(printUtils.printPentity))
                {
                    printUtils.printPentity = value;
                    NotifyPropertyChanged(() => UsePrintPenalties);
                }
                Log.InfoFormat("[默认策略设置] 是否打印自定义惩罚 = {0}.", printUtils.printPentity);
            }
        }

    }
}
